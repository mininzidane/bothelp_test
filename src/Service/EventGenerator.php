<?php

declare(strict_types=1);

namespace App\Service;

class EventGenerator
{
    private $accountId = 1;

    private const EVENT_PER_ACCOUNT_MAX = 10;
    private const EVENTS = [
        'Register',
        'Login',
        'Logout',
        'Fail',
        'Come back',
    ];

    public function generateBatch(): array
    {
        $output = [];
        $count = \random_int(1, self::EVENT_PER_ACCOUNT_MAX);
        $event = self::EVENTS[\array_rand(self::EVENTS)];
        for ($i = 1; $i <= $count; $i++) {
            $output[] = [
                'account' => $this->accountId,
                'event' => "{$event} #{$i}",
            ];
        }
        $this->accountId++;

        return $output;
    }
}
