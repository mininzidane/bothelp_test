<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Queue;

class EventConsumer
{
    public function consume(Queue $queue): void
    {
        // some logic here
        \sleep(1);
    }
}
