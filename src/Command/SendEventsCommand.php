<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Queue;
use App\Service\EventGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendEventsCommand extends Command
{
    private const EVENT_COUNT = 10000;

    protected static $defaultName = 'app:send:events';
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var EventGenerator
     */
    private $eventGenerator;

    public function __construct(
        EntityManagerInterface $em,
        EventGenerator $eventGenerator,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->em = $em;
        $this->eventGenerator = $eventGenerator;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $count = 0;
        while (true) {
            $iterationCount = $this->addToQueue();
            $output->writeln("Adding {$iterationCount} events");
            $count += $iterationCount;
            if ($count >= self::EVENT_COUNT) {
                break;
            }
        }
        return 0;
    }

    private function addToQueue(): int
    {
        $data = $this->eventGenerator->generateBatch();
        foreach ($data as $row) {
            $queue = new Queue();
            $queue->setBody($row);
            $this->em->persist($queue);
        }
        $this->em->flush();

        return \count($data);
    }
}
