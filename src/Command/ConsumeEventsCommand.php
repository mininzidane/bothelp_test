<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Queue;
use App\Repository\QueueRepository;
use App\Service\EventConsumer;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsumeEventsCommand extends Command
{
    private const BATCH_SIZE = 100;
    private const EXIT_CODE_UNKNOWN_ERROR = 1;

    protected static $defaultName = 'app:consume:events';
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var EventConsumer
     */
    private $eventConsumer;

    public function __construct(
        EntityManagerInterface $em,
        EventConsumer $eventConsumer
    )
    {
        parent::__construct(null);
        $this->em = $em;
        $this->eventConsumer = $eventConsumer;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var QueueRepository $repository */
        $repository = $this->em->getRepository(Queue::class);

        do {
            $queueData = $repository->findBy(['isLocked' => false], ['id' => 'ASC'], self::BATCH_SIZE);
            $count = \count($queueData);

            foreach ($queueData as $queue) {
                $queue->setIsLocked(true);
                $this->em->persist($queue);
            }
            $this->em->flush();

            $this->em->beginTransaction();
            try {
                foreach ($queueData as $queue) {
                    $this->em->lock($queue, LockMode::PESSIMISTIC_WRITE);
                    $body = $queue->getBody();
                    $date = $queue->getCreatedAt() ? $queue->getCreatedAt()->format('d.m.Y H:i:s') : null;
                    $output->writeln("Consuming for account #{$body['account']} received at {$date}, event '{$body['event']}'");
                    $this->eventConsumer->consume($queue);
                    $this->em->remove($queue);
                }

                $this->em->flush();
                $this->em->commit();

            } catch (\Throwable $e) {
                $output->writeln("Error: {$e->getMessage()}");
                $this->em->rollback();
                foreach ($queueData as $queue) {
                    $queue->setIsLocked(false);
                    $this->em->persist($queue);
                }
                $this->em->flush();
                $this->em->close();

                return self::EXIT_CODE_UNKNOWN_ERROR;
            }
            $output->writeln("Consumed {$count} queued items");

        } while ($count === self::BATCH_SIZE);

        return 0;
    }
}
