## Initial steps

1. Run `docker-compose up -d --build`
    - run `docker-compose exec php composer i`
    - run `docker-compose exec php bin/console doctrine:database:create`
2. Run migrations `docker-compose exec php bin/console doctrine:migrations:migrate`
3. Emulations:
    - to emulate event sending run command `docker-compose exec php bin/console app:send:events`
    - to emulate event consuming run command `docker-compose exec php bin/console app:consume:events`
    - use as many as u can console instances to run consumer command for better performance